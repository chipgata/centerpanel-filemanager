<?php

/*
 * This file is part of the FileGator package.
 *
 * (c) Milos Stojanovic <alcalbg@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE file
 */

namespace Filegator\Controllers;

use Filegator\Kernel\Request;
use Filegator\Kernel\Response;
use Filegator\Services\Auth\AuthInterface;
use Filegator\Services\Auth\User;
use Filegator\Services\Logger\LoggerInterface;
use Rakit\Validation\Validator;
use \Firebase\JWT\JWT;
use Filegator\Config\Config;
use Filegator\Services\Session\SessionStorageInterface as Session;

class SsoController
{
    protected $logger;
    const SESSION_KEY = 'json_auth';

    public function __construct(LoggerInterface $logger, Config $config, Session $session)
    {
        $this->logger = $logger;
        $this->config = $config;
        $this->session = $session;
    }

    public function auth(Request $request, Response $response)
    {
        $jwt = $request->input('token');
        $this->logger->log("token request ".$jwt);
        $key = $this->config->get('center_panel_jwt_key');
        if(!$jwt)
            return $response->json('Login failed, please try again.0', 422);

        $user = (array)JWT::decode($jwt, $key, array('HS256'));
        if(!$user)
            return $response->json('Login failed, please try again.1', 422);

        $new = new User();
        $new->setUsername($user['username']);
        $new->setName($user['name']);
        $new->setRole($user['role']);
        $new->setHomedir($user['homedir']);
        $new->setPermissions($user['permissions'], true);

        $this->session->set(self::SESSION_KEY, $new);

        return $response->redirect('http://localhost:8080');
    }
}
